FROM ubuntu:20.04
RUN apt-get update && apt-get install -y curl &&  apt-get install zip unzip -y && apt-get install vim -y
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl && chmod +x ./kubectl && mv ./kubectl /usr/local/bin/kubectl
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && unzip awscliv2.zip && ./aws/install
RUN echo 'alias ll="ls -lhrt"' >> /.bashrc  && echo 'alias kubex="kubectl config get-contexts"' >> /.bashrc && echo 'alias kubens="kubectl get namespaces"' >> /.bashrc && echo 'alias kubes="kubectl config use-context"' >> /.bashrc && echo 'alias kubesns="kubectl config set-context --current --namespace"' >> /.bashrc && echo 'alias kubecns="kubectl config view --minify --output 'jsonpath={..namespace}'"' >> /.bashrc
